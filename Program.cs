﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using deber.clases;

namespace deber
{
   public class Program
    {
        static void Main(string[] args)
        {
            Cliente cl = new Cliente();
            Gasolina ga = new Gasolina();

            Console.WriteLine("Ingrese apellidos");
            cl.Apellidos = Console.ReadLine();
            Console.WriteLine("Ingrese nombres");
            cl.Nombres = Console.ReadLine();
            Console.WriteLine("Ingrese cedula de identidad");
            cl.Ci = Console.ReadLine();
            Console.WriteLine("Ingrese direccion");
            cl.Direccion = Console.ReadLine();
            Console.WriteLine("Ingrese tipo de gasolina");
            ga.Tipo = Console.ReadLine();
            Console.WriteLine("Ingrese cantidad de gasolina");
            ga.Cantidad = int.Parse(Console.ReadLine());

            Console.WriteLine("                      Factura                                     ");
            Console.WriteLine("Apellidos y Nombres : " + cl.Apellidos +(" ")+ cl.Nombres);
            Console.WriteLine("Cedula de identidad : " + cl.Ci );
            Console.WriteLine("Direccion : " +cl.Direccion );
            Console.WriteLine("Tipo de Gasolina : "+ga.Tipo);
            Console.WriteLine("Cantidad de Gasolina : "+ga.Cantidad);
            Console.WriteLine("subtotal:                            "+ ga.Subtotal);
            Console.WriteLine("iva:                                 " + ga.Iva);
            Console.WriteLine("Total a Pagar :                      "+ ga.Total);
            Console.ReadKey();
        }
       
    }
   
}
